/**
* Phonegap Zebra Printer Model MZ220 Interface Plugin
* Copyright (c) KPS3 2013
*
*/

package com.phonegap.plugins.zebraprintermz220;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;

import android.os.Looper;
import android.util.Log;
import android.Manifest;
import android.content.pm.PackageManager;

import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.discovery.BluetoothDiscoverer;
import com.zebra.sdk.printer.discovery.DiscoveredPrinter;
import com.zebra.sdk.printer.discovery.DiscoveredPrinterBluetooth;
import com.zebra.sdk.printer.discovery.DiscoveryHandler;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ZebraPrinterMZ220 extends CordovaPlugin {

    private static final String FIND = "findPrinters";
    private static final String DEVICES = "getDevices";
    private static final String OPENCONN = "openConnection";
    private static final String CLOSECONN = "closeConnection";
    private static final String PRINT = "print";

    private Connection connection;
    private Map<String, String> deviceList = new HashMap<String, String>();

    public CallbackContext cbContext;
    private JSONArray args;
    private String action;

    private static DiscoveryHandler btDiscoveryHandler = null;
    private final int REQUEST_ACCESS_COARSE_LOCATION_CODE = 0;

    public ZebraPrinterMZ220() {
        Log.d("ZebraPrinterMZ220", "Plugin created");
    }

    protected void getCoarseLocationPermission(int requestCode) {
        cordova.requestPermission(this, requestCode, Manifest.permission.ACCESS_COARSE_LOCATION);
    }

    public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
        for(int r:grantResults) {
            if(r == PackageManager.PERMISSION_DENIED) {
                this.cbContext.error("permissions");
                return;
            }
        }
        switch(requestCode) {
            case REQUEST_ACCESS_COARSE_LOCATION_CODE:
                Log.d("ZebraPrinterMZ220", "GOT PERMISSION!!");
                execute(this.action, this.args, this.cbContext);
                break;
        }
    }

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        this.cbContext = callbackContext;
        this.action = action;
        this.args = args;

        if (!cordova.hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            getCoarseLocationPermission(REQUEST_ACCESS_COARSE_LOCATION_CODE);
        }

        if(action.equals(OPENCONN)) {
            try{
                Connection thePrinterConn = new BluetoothConnection(args.getString(0));
                setConnection(thePrinterConn);
                thePrinterConn.open();
            }
            catch(Exception e) {
                this.cbContext.success("There was an error connecting to the printer");
            }
        }
        else if(action.equals(CLOSECONN)) {
            try{
                Connection thePrinterConn = getConnection();
                thePrinterConn.close();
                this.cbContext.success("true");
            }
            catch(Exception e) {
                //nothing yet
            }
        }
        else if(action.equals(FIND)) {
            find();
        }
        else if(action.equals(DEVICES)) {
            this.cbContext.success(getDeviceList());
        }
        else if(action.equals(PRINT)) {
            print(args);
        }

        return true;
    }

    public void results(String name, String address) {
        try {
            addDevice(name, address);
        } catch(Exception e) {
            //nothing for now
        }
    }

    public void finished() {
        this.cbContext.success("true");
    }

    public void find(){
        final CallbackContext cbContext = this.cbContext;

        new Thread(new Runnable() {
            public void run() {
                btDiscoveryHandler = new DiscoveryHandler() {

                    public void discoveryError(String message) {

                        cbContext.success("false");
                    }

                    public void discoveryFinished() {

                        cbContext.success("true");
                    }

                    public void foundPrinter(final DiscoveredPrinter printer) {
                        DiscoveredPrinterBluetooth p = (DiscoveredPrinterBluetooth) printer;
                        addDevice(p.friendlyName, p.address);
                    }
                };

                Looper.prepare();
                try {
                    BluetoothDiscoverer.findPrinters(cordova.getActivity(), btDiscoveryHandler);
                } catch (ConnectionException e) {
                    //cbContext.success(e.getMessage());
                } finally {
                    Looper.myLooper().quit();
                }
            }
        }).start();
    }

    public void shipIt() {
        this.cbContext.success();
    }

    public void print(JSONArray args){
      final JSONArray stuff = args;
      final Connection thePrinterConn = getConnection();
      new Thread(new Runnable() {
          public void run() {
              try {

                  // Initialize
                  Looper.prepare();

                  String cpclData = stuff.getString(0);

                  // Send the data to printer as a byte array.
                  thePrinterConn.write(cpclData.getBytes());

              } catch (Exception e) {

                  // Handle communications error here.
                  e.printStackTrace();

              } finally {

                    Looper.myLooper().quit();

                    shipIt();
              }
          }
      }).start();
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getDeviceList() {
        JSONObject obj = new JSONObject(deviceList);
        return obj.toString();
    }

    public void addDevice(String name, String address)
    {
        if(name == null)
            name = "unknown";
        deviceList.put(name, address);
    }
}
